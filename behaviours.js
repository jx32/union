let Types = class {
    /*
        Obtiene HTML para cierto tipo de componente y comportamiento

        @params
        componentName: Tipo de componente (switch, text, range, checkbox)
    */
    constructor (componentName, afterLabel, beforeLabel, id)
    {
            if (componentName == 'number')
            {
                this.id = id
                this.html = afterLabel + ' <input class="form-control" id="beh' + id + '" type="number" value="0" onChange="sendChange(this, ' + id + ')"> ' + beforeLabel
            }
            else if (componentName == 'checkbox')
            {
                this.id = id
                this.html = '<div class="custom-control custom-switch"><input class="custom-control-input" onChange="sendChange(this, ' + id + ')" id="beh' + id + '" type="checkbox" checked="false"><label class="custom-control-label" for="beh' + id + '">' + afterLabel + '</label></div> ' + beforeLabel
            }
            else if (componentName == 'range')
            {
                this.id = id
                this.html = afterLabel + ' <input class="form-control-range" onChange="sendChange(this, ' + id + ')" id="beh' + id + '" type="range" value="0" min="0" max="255"> ' + beforeLabel
            }
        }
}

module.exports = Types