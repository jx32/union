#include <Arduino.h>
#ifndef UNION_h
#define UNION_h

#include "Arduino.h"

class Behaviour
{
  public:
    Behaviour();

    void execute();
    void limpiar();
    
    void setDato(String input);
    void setEspera(int espera);
    void setPinOut(int pin);
    String getDato();
    int getEspera();
    int getPinOut();
  private:
    int wait;
    int pinOut;
    String dato;

    int mapOutput();
};

#endif
