#include "UNION.h"

Behaviour::Behaviour()
{
  return;
}

void Behaviour::setDato(String input)
{
  dato = input;
}

void Behaviour::setEspera(int espera)
{
  wait = espera;
}

void Behaviour::setPinOut(int pin)
{
  pinOut = pin;
}

String Behaviour::getDato()
{
  return dato;
}

int Behaviour::getEspera()
{
  return wait;
}

int Behaviour::getPinOut()
{
  return pinOut;
}

int Behaviour::mapOutput()
{
  // Se tiene la constante de que la aplicación web siempre
  // enviará de 0 a 255 por lo que se debe mapear con el pin
  // para ajustarlo a PWM o no.
  if (pinOut < 2 || pinOut > 5)
  {
    if (dato.toInt() > 0)
    {
      return HIGH;
    }
    else
    {
      return LOW;
    }
  }
  else
  {
    return dato.toInt();
  }
}

void Behaviour::execute()
{
  int mappedOut = mapOutput();
 
  if (wait > 0)
    delay(wait);

  if (pinOut < 2 || pinOut > 5)
  {
    digitalWrite(pinOut, mappedOut);
  }
  else
  {
    analogWrite(pinOut, mappedOut);
  }
}

void Behaviour::limpiar()
{
  wait = 0;
  pinOut = 0;
  dato = "";
}
