/*
 * UNION project es:
 * Aaron Macías
 * Aldo Fernández
 * Jorge Fernández
 * Orlando Carreon
 * Royce Fernández
 * 
 * versión 1.0
 * En esta versión se soportan 4 sensores,
 * 3 conmutaciones y 3 salidas PWM.
 */

#include <Firebase_Arduino_WiFiNINA.h>
#include <WiFiNINA.h>
#include <SPI.h>
#include <ArduinoBLE.h>
#include "UNION.h"
// Librerias de sensores
#include "dht.h"

dht DHT;

const bool IN_DEBUG = false;
const String ver = "1.0";

bool retryConnect = true;
bool retryRead = true;
bool profile = false;

int pinPwm5 = 5;
int pinPwm4 = 4;
int pinPwm3 = 3;
int pinPwm2 = 2;
int pinAnalogico0 = A0;
int pinAnalogico1 = A1;
int pinAnalogico2 = A2;
int pinAnalogico3 = A3;
int pinAnalogico4 = A4;
int pinAnalogico5 = A5;
int pinAnalogico6 = A6;
int pin0 = 0;
int pin1 = 1;
int pin2 = 2;
int pin3 = 3;
int pin4 = 4;
int pin5 = 5;
int pin6 = 6;
int pin7 = 7;
int pin8 = 8;
int pin9 = 9;
int pin10 = 10;
int pin11 = 11;
int pin12 = 12;
int pin13 = 13;
int pin14 = 14;

int retryTimes = 3;
const int TIMES_BETWEEN_SENSOR = 200;
const int TIMES_BETWEEN_BEH = 50;
const int TIMES_BETWEEN_TRIG = 49;
const int TIMES_BETWEEN_FLAG = 1000;
int ACTUAL_TBS = 1;
int ACTUAL_TBB = 1;
int ACTUAL_TBT = 1;
int ACTUAL_TBF = 1;

const char* ssid = "Tsingtao";
const char* password = "meljo25051410";

String firebaseHost = "union-project-750ae.firebaseio.com";
String firebaseAuth = "ybR0qWzOLLyU3cJoH10nFm3P05qxqOPeL0P3mu8y";

FirebaseData firebase;

/*BLEDevice central;
BLEService unionService("19B10000-E8F2-537E-4F6C-D104768A1214");
BLEByteCharacteristic wifiCharacteristic("19B10001-E8F2-537E-4F6C-D104768A1214", BLERead | BLEWrite);*/

String fabricant = "lg-1"; // usado para obtener perfil de acuerdo a fabricante
String uniqueId = "test"; // usado para leer y escribir en ID de dispositivo

// Sensores
const int PIN_MAX_SENSORS = 0;
String type_sensor1;
String type_sensor2;
int sensor1_pins[PIN_MAX_SENSORS+1];
int sensor2_pins[PIN_MAX_SENSORS+1];
int sensor1Value;
int sensor2Value;

// Conmutaciones / Comportamientos
Behaviour behavioursArray[10];
int TOTAL_BEHAVIOURS;

// Triggers
int TOTAL_TRIGGERS;
String tCompareObj[10];
String tCompareType[10];
String tOperator[10];
String tCondition[10];

String tEvBehaviour[10];
int tEvDelay[10];
String tEvType[10];
String tEvFlexField1[10];
String tEvFlexField2[10];
String tEvFlexField3[10];
String tEvFlexField4[10];
String tEvFlexField5[10];
String tEvLastChecksum[10];

/*
   Funciones auxiliares
*/
void copyArray(int* src, int* dst, int len) {
    for (int i = 0; i <= len; i++) {
        *dst++ = *src++;
    }
}

// Función que regresa el valor leído de una forma especifica para un sensor
String sensor(String IDsensor, String tipoSensor)
{
  int sensorPins[PIN_MAX_SENSORS];
  
  int specialVar1;
  int specialVar2;
  String bufffer;
  
  // Definir array de pines a usar en función del ID del sensor
  if (IDsensor == "sensor1")
  {
    copyArray(sensor1_pins, sensorPins, PIN_MAX_SENSORS);
  }
  else if (IDsensor == "sensor2")
  {
    copyArray(sensor2_pins, sensorPins, PIN_MAX_SENSORS);
  }

  // Leer valores
  // Temperatura y humedad
  
  if (tipoSensor == "DHT11")
  {
    float humi;
    float temp;
    
    DHT.read11(sensorPins[0]);

    temp = DHT.temperature;
    
    bufffer = String(temp);
  }

  // Sensor generico con una sola entrada de datos, HIGH o LOW
  if (tipoSensor == "GENERIC")
  { 
    bufffer = digitalRead(sensorPins[0]);
  }

  if (IDsensor == "sensor1")
  {
    sensor1Value = bufffer.toInt();
  }
  else if (IDsensor == "sensor2")
  {
    sensor2Value = bufffer.toInt();
  }

  Serial.println("[SENSOR][" + IDsensor + "] Tipo: " + tipoSensor + ", Leído: " + bufffer);

  return bufffer;
}
/**************************/

/*
   Funciones para actualizar base de datos
*/
void actualizarSensor(String IDsensor, String datos)
{
  IDsensor.toLowerCase();

  Serial.print("[FIREBASE] Actualizando objeto del sensor id: " + IDsensor + " con el dato: " + datos + " en base de datos... ");

  if (Firebase.setString(firebase, "/devices/" + uniqueId + "/data/" + IDsensor, datos)) {
    Serial.println("OK");
  }
  else
  {
    Serial.println(firebase.errorReason());
  }
}
/**************************/

// Función encargada de definir pins de sensores,
// re-definir salidas o entradas,
// llamar métodos propios del sensor.
void confEspSensores()
{
    if (type_sensor1 == "DHT11")
    {
      sensor1_pins[0] = pin6;
    }
    else if (type_sensor2 == "DHT11")
    {
      sensor2_pins[0] = pin7;
    }

    if (type_sensor1 == "GENERIC")
    {
      sensor1_pins[0] = pin6;
    }
    else if (type_sensor2 == "GENERIC")
    {
      sensor2_pins[0] = pin7;
    }
}

void leerBehaviours()
{ 
  ACTUAL_TBB += 1;
  
  if (ACTUAL_TBB == TIMES_BETWEEN_SENSOR)
  {
    ACTUAL_TBB = 1;
    
    if (WiFi.status() == WL_CONNECTED && profile)
    {
      for (int i = 1; i <= TOTAL_BEHAVIOURS; i++)
      {
        if (Firebase.getString(firebase, "/devices/" + uniqueId + "/data/" + String(i)))
        {
           behavioursArray[i].setDato(firebase.stringData());
           behavioursArray[i].execute();
  
           Serial.println("[FIREBASE][Behaviour #" + String(i) + "] " + behavioursArray[i].getDato() + " - Pin: " + behavioursArray[i].getPinOut());
        }
        else
        {
          Serial.println(firebase.errorReason());
        }
      }
    }
  }
}

void definirES()
{
  pinMode(pin11, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  // PWMs
  pinMode(pinPwm2, OUTPUT);
  pinMode(pinPwm3, OUTPUT);
  pinMode(pinPwm4, OUTPUT);
  pinMode(pinPwm5, OUTPUT);
  // Conmutaciones
  pinMode(pin12, OUTPUT);
  pinMode(pin13, OUTPUT);
  pinMode(pin14, OUTPUT);
  // Sensores
  // Sensor 1
  pinMode(pin6, INPUT);
  pinMode(pin7, INPUT);
  pinMode(pin8, INPUT);
  pinMode(pin9, INPUT);
  // Sensor 2
  pinMode(pin0, INPUT);
  pinMode(pin1, INPUT);
  pinMode(pin10, INPUT);
  
}

void leerSensores()
{
  ACTUAL_TBS += 1;
  
  if (ACTUAL_TBS == TIMES_BETWEEN_SENSOR)
  {
    ACTUAL_TBS = 1;
    
    if (WiFi.status() == WL_CONNECTED && profile)
    {
      if (type_sensor1 != "")
      {
        actualizarSensor("sensor1", sensor("sensor1", type_sensor1));
      }
  
      if (type_sensor2 != "")
      {
        actualizarSensor("sensor2", sensor("sensor2", type_sensor2));
      }
    }
  }
}

void leerPerfil()
{
  // Medición de rendimiento
  unsigned long StartTime = millis();

  Serial.println("[FIREBASE] Leyendo perfil: " + fabricant);

  Serial.print("[FIREBASE][PERFIL] Versión: ");
  
  if (Firebase.getString(firebase, "/profiles/" + fabricant + "/version")) {
    Serial.println(firebase.stringData());
  }

  Serial.print("[FIREBASE][PERFIL] Obteniendo total de comportamientos... ");

  for (int i = 0; i < 10; i++)
  {
    behavioursArray[i].limpiar();
  }

  TOTAL_BEHAVIOURS = 0;

  if (Firebase.getInt(firebase, "/profiles/" + fabricant + "/behaviours/enummBehaviours")) {
    TOTAL_BEHAVIOURS = firebase.intData();
    Serial.print("*");
  } else {
    Serial.println("ERROR, " + firebase.errorReason());
  }
  Serial.println(" OK");

  Serial.print("[FIREBASE][PERFIL] Poblando disparadores... ");

  for (int i = 0; i < 10; i++)
  {
    tCompareObj[i] = "";
    tCompareType[i] = "";
    tOperator[i] = "";
    tCondition[i] = "";
    tEvBehaviour[i] = "";
    tEvDelay[i] = 0;
    tEvType[i] = "";
    tEvFlexField1[i] = "";
    tEvFlexField2[i] = "";
    tEvFlexField3[i] = "";
    tEvFlexField4[i] = "";
    tEvFlexField5[i] = "";
    tEvLastChecksum[i] = "";
  }

  TOTAL_TRIGGERS = 0;

  if (Firebase.getInt(firebase, "/profiles/" + fabricant + "/triggers/enummTriggers")) {
    TOTAL_TRIGGERS = firebase.intData();
    Serial.print("*");
  } else {
    Serial.println("ERROR, " + firebase.errorReason());
  }

  for (int x = 1; x <= TOTAL_TRIGGERS; x++){
    if (Firebase.getString(firebase, "/profiles/" + fabricant + "/triggers/" + String(x) + "/compareObj")) {
      tCompareObj[x-1] = firebase.stringData();
      Serial.print(" *");
    } else {
      Serial.println("ERROR, " + firebase.errorReason());
    }

    if (Firebase.getString(firebase, "/profiles/" + fabricant + "/triggers/" + String(x) + "/condition")) {
      tCondition[x-1] = firebase.stringData();
      Serial.print("*");
    } else {
      Serial.println("ERROR, " + firebase.errorReason());
    }

    if (Firebase.getString(firebase, "/profiles/" + fabricant + "/triggers/" + String(x) + "/objType")) {
      tCompareType[x-1] = firebase.stringData();
      Serial.print("*");
    } else {
      Serial.println("ERROR, " + firebase.errorReason());
    }

    if (Firebase.getString(firebase, "/profiles/" + fabricant + "/triggers/" + String(x) + "/operator")) {
      tOperator[x-1] = firebase.stringData();
      Serial.print("*");
    } else {
      Serial.println("ERROR, " + firebase.errorReason());
    }

    if (Firebase.getString(firebase, "/profiles/" + fabricant + "/triggers/" + String(x) + "/event/behaviour")) {
      tEvBehaviour[x-1] = firebase.stringData();
      Serial.print("*");
    } else {
      Serial.println("ERROR, " + firebase.errorReason());
    }

    if (Firebase.getInt(firebase, "/profiles/" + fabricant + "/triggers/" + String(x) + "/event/delay")) {
      tEvDelay[x-1] = firebase.intData();
      Serial.print("*");
    } else {
      Serial.println("ERROR, " + firebase.errorReason());
    }
    
    if (Firebase.getString(firebase, "/profiles/" + fabricant + "/triggers/" + String(x) + "/event/type")) {
      tEvType[x-1] = firebase.stringData();
      Serial.print("*");
    } else {
      Serial.println("ERROR, " + firebase.errorReason());
    }

    if (Firebase.getString(firebase, "/profiles/" + fabricant + "/triggers/" + String(x) + "/event/flexfield1")) {
      tEvFlexField1[x-1] = firebase.stringData();
      Serial.print("*");
    } else {
      Serial.println("ERROR, " + firebase.errorReason());
    }    

    if (Firebase.getString(firebase, "/profiles/" + fabricant + "/triggers/" + String(x) + "/event/flexfield2")) {
      tEvFlexField2[x-1] = firebase.stringData();
      Serial.print("*");
    } else {
      Serial.println("ERROR, " + firebase.errorReason());
    }

    if (Firebase.getString(firebase, "/profiles/" + fabricant + "/triggers/" + String(x) + "/event/flexfield3")) {
      tEvFlexField3[x-1] = firebase.stringData();
      Serial.print("*");
    } else {
      Serial.println("ERROR, " + firebase.errorReason());
    }    
    
    if (Firebase.getString(firebase, "/profiles/" + fabricant + "/triggers/" + String(x) + "/event/flexfield4")) {
      tEvFlexField4[x-1] = firebase.stringData();
      Serial.print("*");
    } else {
      Serial.println("ERROR, " + firebase.errorReason());
    }    

    if (Firebase.getString(firebase, "/profiles/" + fabricant + "/triggers/" + String(x) + "/event/flexfield5")) {
      tEvFlexField5[x-1] = firebase.stringData();
      Serial.print("* ");
    } else {
      Serial.println("ERROR, " + firebase.errorReason());
    }    
  }
  Serial.println(" OK");

  Serial.print("[FIREBASE][PERFIL] Poblando objetos comportamientos...");

  for (int i = 1; i <= TOTAL_BEHAVIOURS; i++)
  {
    if (Firebase.getInt(firebase, "/profiles/" + fabricant + "/behaviours/" + String(i) + "/delay")) {
      behavioursArray[i].setEspera(firebase.intData());
      Serial.print(" *");
    } else {
      Serial.println("ERROR, " + firebase.errorReason());
    }
    if (Firebase.getInt(firebase, "/profiles/" + fabricant + "/behaviours/" + String(i) + "/pinOut")) {
      behavioursArray[i].setPinOut(firebase.intData());
      Serial.print("*");
    } else {
      Serial.println("ERROR, " + firebase.errorReason());
    }
  }
  Serial.println(" OK");

  Serial.print("[FIREBASE][PERFIL] Poblando tipos de sensores... ");

  type_sensor1 = "";
  type_sensor2 = "";

  if (Firebase.getString(firebase, "/profiles/" + fabricant + "/behaviours/sensor1/type")) {
    type_sensor1 = firebase.stringData();
    type_sensor1.toUpperCase();
    Serial.print("*");
  } else {
    Serial.println("ERROR, " + firebase.errorReason());
  }

  if (Firebase.getString(firebase, "/profiles/" + fabricant + "/behaviours/sensor2/type")) {
    type_sensor2 = firebase.stringData();
    type_sensor2.toUpperCase();
    Serial.print("*");
  } else {
    Serial.println("ERROR, " + firebase.errorReason());
  }
  Serial.println(" OK");

  Serial.print("[FIREBASE][PERFIL] Configurando sensores... ");
  
  for (int i = 0; i <= PIN_MAX_SENSORS; i++)
  {
    sensor1_pins[i] = 0;
    sensor2_pins[i] = 0;
    
  }
  
  confEspSensores();
  Serial.println(" OK");

  profile = true;

  // Medir rendimiento
  unsigned long CurrentTime = millis();
  unsigned long ElapsedTime = CurrentTime - StartTime;

  Serial.print("[FIREBASE][PERFIL] Se ha terminado de leer el perfil. Duración: ");
  Serial.print(ElapsedTime);
  Serial.println("ms.");

  // Bandera de UNION preparado
  analogWrite(LED_BUILTIN, 100);
}

void iniciarFirebase()
{
  if (WiFi.status() == WL_CONNECTED)
  {
    Firebase.begin(firebaseHost, firebaseAuth, ssid, password);

    Serial.println("[FIREBASE] Conectado con exito.");

    leerPerfil();
  }
  else
  {
    Serial.println("[FIREBASE] No se ha podido iniciar debido a estatus de WIFI");
  }
}

void conectarBLUETOOTH() {
  /*if (BLE.connected() == false) {

    if (BLE.begin()) {
      Serial.println("[BLE] Bluetooth Iniciado");

      BLE.setLocalName("union-" + random(10, 99));
      BLE.setAdvertisedService(unionService);

      unionService.addCharacteristic(wifiCharacteristic);
      BLE.addService(unionService);
      BLE.advertise();

      Serial.println("[BLE] Union Peripheral started");
    }
    else {

      Serial.println("[BLE] No se ha podido iniciar BLE");
    }
  }*/
}



void notificarConexionWIFI(int mensaje)
{
  /*Serial.print("[BLE] Notificando a Central... ");
  if (central.connected())
  {
    if (wifiCharacteristic.writeValue(mensaje))
    {
      Serial.println("OK");
    }
    else
    {
      Serial.println("ERROR, no se ha podido escribir");
    }
  }
  else
  {
    Serial.println("ERROR, Central no está conectado");
  }

  // Se anula conexión pasada definiendole un nuevo objeto
  central = BLE.central();*/
}

void conectarWIFI() {
  if (WiFi.status() != WL_NO_MODULE && WiFi.status() != WL_CONNECTED && retryConnect && strlen(ssid) > 0) {
    int conteo = 0;

    while (WiFi.status() != WL_CONNECTED) {
      Serial.println("[WIFI] Intentando conectar via WIFI...");
      conteo += 1;
      WiFi.begin(ssid, password);

      delay(1000);

      if (conteo == retryTimes && WiFi.status() != WL_CONNECTED) {
        Serial.println("[WIFI] Tiempo de espera agotado para conexion WIFI");
        notificarConexionWIFI(0);
        retryConnect = false;
        retryRead = true;
        return;
      }
      else if (WiFi.status() == WL_CONNECTED)
      {
        Serial.println ("[WIFI] Conexion WIFI exitosa");
        notificarConexionWIFI(1);
        retryRead = true;

        iniciarFirebase();
      }
    }
  }
  else if (WiFi.status() == WL_NO_MODULE)
  {
    Serial.println("[WIFI] No se ha encontrado modulo WIFI");
    notificarConexionWIFI(0);
    retryRead = true;
  }

}

void leerBLE()
{
  /*if (central && retryRead)
  {
    Serial.println("[BLE] Conectado con dispositivo CENTRAL.");
    Serial.println("[BLE] Esperando credenciales WIFI...");

    while (central.connected())
    {
      if (wifiCharacteristic.written())
      {
        // Obtener valor de característica
        String datosLeidos = String(wifiCharacteristic.value());

        if (datosLeidos.indexOf("|") > 0)
        {
          // Separar ssid y password de cadena leída
          String v_ssid = datosLeidos.substring(1, datosLeidos.indexOf("|") + 2);
          String v_pass = datosLeidos.substring(datosLeidos.indexOf("|") + 2);

          // Re convertir a tipo dato const char* y definirlo a variables globales
          ssid = v_ssid.c_str();
          password = v_pass.c_str();

          Serial.println("SSID leido: " + v_ssid);
          Serial.println("PASSWORD leido: " + v_pass);

          // Bandera para no volver a entrar en bucle a esperar datos entrantes.
          // En este punto se espera la respuesta resultante de conexión con WIFI
          // para volver a leer datos de BLE.
          retryRead = false;
        }
        else
        {
          Serial.println("[BLE] Cadena de entrada invalida: " + datosLeidos);
        }

        return;
      }
    }

    Serial.println("[BLE] No se recibieron credenciales nuevas.");
    Serial.println("-- ACTUALES --");
    Serial.println(ssid);
    Serial.println(password);
  }*/
}

boolean ejecutarComparacion(String operador, int value1, int value2)
{
  operador.toLowerCase();
  
  if (operador == "equal")
  {
    return (value1 == value2);
  }
  else if (operador == "major")
  {
    return (value1 > value2);
  }
  else if (operador == "majororequal")
  {
    return (value1 >= value2);
  }  
  else if (operador == "minor")
  {
    return (value1 < value2);
  }  
  else if (operador == "minororequal")
  {
    return (value1 <= value2);
  }  
  else if (operador == "different")
  {
    return (value1 != value2);
  }  
}

// Función que actualiza congelamiento para control web
void congelamientoBehaviourWeb(bool enabled, int behIdentificador){
  if (!Firebase.setBool(firebase, "/devices/" + uniqueId + "/data/" + behIdentificador + "Enabled", enabled)) {
    Serial.println(firebase.errorReason());
  }
}

// Función que ejecuta evento depende a su tipo
void ejecutarEvento(int index)
{ 
  Behaviour comportamiento = behavioursArray[ tEvBehaviour[index].toInt() ];

  tEvType[index].toLowerCase();

  if (tEvType[index] == "fireonfire")
  {
    // fireOnFire
    // Consiste en conmutar intensidades de señal durante un tiempo determinado.
    // FlexField1 = Primera señal a conmutar
    // FlexField2 = Segunda señal a conmutar
    
    comportamiento.setDato(tEvFlexField1[index]);
    comportamiento.execute();
    delay(tEvDelay[index]);
    comportamiento.setDato(tEvFlexField2[index]);
    comportamiento.execute();
  }

  if (tEvType[index] == "web-notification")
  {
    // web-notification
    // Consiste en enviar una notificación web a un usuario
    // FlexField1 = Titulo
    // FlexField2 = Cuerpo
    
    delay(tEvDelay[index]);
    
  }
}

void leerTriggers()
{
  ACTUAL_TBT += 1;
  
  if (ACTUAL_TBT == TIMES_BETWEEN_TRIG)
  {
    ACTUAL_TBT = 1;
  
    if (TOTAL_TRIGGERS > 0)
    {
      for (int x = 0; x < TOTAL_TRIGGERS; x++)
      {
        // Poblar objetos para realizar comprobación
        int comparingObj;
        int conditionalObj = tCondition[x].toInt();
  
        if (tCompareType[x] == "behaviour")
        {
          comparingObj = behavioursArray[ tCompareObj[x].toInt() ].getDato().toInt();
        }
        else if (tCompareType[x] == "sensor")
        {
          if (tCompareObj[x] == "sensor1")
          {
            comparingObj = sensor1Value;
          }
          if (tCompareObj[x] == "sensor2")
          {
            comparingObj = sensor2Value;
          }
        }

        if (ejecutarComparacion(tOperator[x], comparingObj, conditionalObj))
        {
          if ( String(comparingObj + conditionalObj) != tEvLastChecksum[x] )
          {
            tEvLastChecksum[x] = String(comparingObj + conditionalObj);
            
            Serial.println("[SYSTEM][TRIGGERS] Comparación resultó verdadera. Checksum nuevo: " + tEvLastChecksum[x]);

            congelamientoBehaviourWeb(false, x+1);
            
            ejecutarEvento(x);
          }
        }
        else
        {
          tEvLastChecksum[x] = "";
        }

        Serial.println("[SYSTEM][TRIGGERS] Comparando " + String(comparingObj) + " " + tOperator[x] + " con: " + String(conditionalObj) + " Checksum: " + tEvLastChecksum[x]);
      }

      for (int x = 1; x <= TOTAL_TRIGGERS; x++)
      {
        congelamientoBehaviourWeb(true, x);
      }
    }
  }
}

void leerFlags()
{
  ACTUAL_TBF += 1;
  
  if (ACTUAL_TBF == TIMES_BETWEEN_FLAG)
  {
    ACTUAL_TBF = 1;
    
    if (WiFi.status() == WL_CONNECTED)
    {
      Serial.println("[SYSTEM][FLAGS] Leyendo banderas...");
      
      if (Firebase.getBool(firebase, "/devices/" + uniqueId + "/flags/rewriteProfile")) {
        if (firebase.boolData())
        {
          Serial.println("[SYSTEM][FLAGS] Se volverá a leer perfil...");
          
          if (Firebase.setBool(firebase, "/devices/" + uniqueId + "/flags/rewriteProfile", false)) {
            profile = false;
  
            leerPerfil();
          }
          else
          {
            Serial.println(firebase.errorReason());
          }
        }
      } else {
        Serial.println("ERROR, " + firebase.errorReason());
      }
    }
  }
}

void setup() {
  Serial.begin(9600);

  if (IN_DEBUG)
  {
    while (!Serial) {
      ;
    }
  }

  Firebase.reconnectWiFi(true);
  
  definirES();
  //conectarBLUETOOTH();
  
  for (int i = 0; i < 10; i++)
  {
    behavioursArray[i] = Behaviour();
  }

  //central = BLE.central();

  Serial.println("[SYSTEM][INFO] UNION iniciado correctamente.");
  Serial.println("[SYSTEM][INFO] Versión: " + ver);
}

void loop() {
  leerBLE();
  conectarWIFI();
  
  leerSensores();
  leerBehaviours();
  leerTriggers();
  leerFlags();
  delay(10);
}
