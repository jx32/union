require('dotenv').config()

var express = require('express')
var exphbs = require('express-handlebars')
var bodyParser = require('body-parser')
var cors = require('cors')
var app = express()
var firebase = require('firebase')
const { check, validationResult, sanitizeBody, body } = require('express-validator');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path')
const webpush = require('web-push')

app.engine('.hbs', exphbs({extname: '.hbs'}))
app.set('views', './views')
app.set('view engine', '.hbs')

app.use(bodyParser.urlencoded({ extended:true }))
app.use(express.static(path.join(__dirname, '/public')))
app.use(cors())
app.use(express.json())

const firebaseConfig = {
    apiKey: "AIzaSyCWGfc9cyyQ2sJjWA-9ziBoIdiN1w68Slo",
    authDomain: "union-project-750ae.firebaseapp.com",
    databaseURL: "https://union-project-750ae.firebaseio.com",
    projectId: "union-project-750ae",
    storageBucket: "union-project-750ae.appspot.com",
    messagingSenderId: "191904133600",
    appId: "1:191904133600:web:a4a3e0ea1700bc555f3b85",
    measurementId: "G-LRZ9CXJGSM"
  }

firebase.initializeApp(firebaseConfig)

const auth = firebase.auth()
const db = firebase.database()

var lastData;
var refDevice;

webpush.setVapidDetails('https://unionapp.herokuapp.com/', process.env.WEBPUSH_PUBLIC_KEY, process.env.WEBPUSH_PRIVATE_KEY)

http.listen(process.env.PORT || 3000, () => console.log('UNION 1.0 server started'))

io.on('connection', (socket) =>{
    socket.emit('data', lastData)

    socket.on('update', function(data){
        refDevice.child(data['id']).set(data['value'])
    })
   })

app.post('/subscription/:id', async (req, res) => {
    res.status(200).json()

    await db.ref('/users/' + req.params.id + '/subscription')
        .set(req.body);
})

app.get('/specialLogin', async (req, res) => {
    // poner un usuario fijo
    await auth.signInAnonymously().then(function(){
        res.redirect('device/test')
    }).catch(function(error){
        res.send(error)
    })
})

app.get('/device/:id', (req, res) => {
    var user = auth.currentUser
    var email
    var uid
    var sensorsData = require('./sensors')
    var Types = require('./behaviours')
    var sensores = []
    var comportamientos = []

    const MAX_SENSORS = 4

    if (user){
        email = user.email
        uid = user.uid
        //uid = '9HuktqVbegRVaTyln0tYYc7jFyC2'
        
        db.ref('/users/' + uid + '/devicesProfiles').once('value').then(function(snapshot){
            snapshot.val().forEach(function(element, index, array){
                var device = element['device']
                var profile = element['profile']

                deviceId = device


                // Consultar datos del perfil
                db.ref('/profiles/' + profile + '/behaviours').once('value').then(function(snapshot){

                    // Componentes de Sensores
                    for (x = 0; x <= MAX_SENSORS; x++)
                    {
                        var sensorId = snapshot.val()['sensor' + x]
                    
                        if( sensorId != null )
                        {
                            var composedId = { id: x, label: sensorId['web-label'] }
                            var composedObj = Object.assign(sensorsData[sensorId['type']], composedId)
                            sensores.push(composedObj)
                        }
                    }

                    // Componentes de comportamientos
                    var countBehaviours = parseInt(snapshot.val()['enummBehaviours'])

                    for (x = 1; x <= countBehaviours; x++)
                    {
                        var behId = x
                        var behType = snapshot.val()[x]['web-type']
                        var behLabel = snapshot.val()[x]['web-label']
                        const DEFAULT_BEH_TYPE = 'number'
                    
                        if( behId != null && behType != 'hidden')
                        {
                            if (behType == null || behType == '')
                                behType = DEFAULT_BEH_TYPE

                            var typeObj = new Types(behType, behLabel, '', behId)
                            var composedObj = { codigo: typeObj.html, id: typeObj.id }
                            comportamientos.push(composedObj)
                        }

                    }

                    refDevice = db.ref('/devices/' + device + '/data')
                    refDevice.on('value', function(snapshot){
                        lastData = snapshot.val()
                        io.emit('data', lastData)
                    })

                }).then(function(){
                    //console.log({user: email, id: uid, sensorsObj: sensores, behavioursObj: comportamientos})
                    res.render('device', {user: email, id: uid, sensorsObj: sensores, behavioursObj: comportamientos})
                })  
            })
        })

        
    }
    else
    {
        res.redirect('/')
    }
})

app.get('/', (req, res) => {
    if (auth.currentUser){
        res.redirect('/home')
    }
    else
    {
        res.render('home')
    }
})

app.get('/home', (req, res) => {
    var user = auth.currentUser
    var email
    var uid
    var perfil = []

        if (user)
        {
            email = user.email
            uid = user.uid
            //uid = '9HuktqVbegRVaTyln0tYYc7jFyC2'

            db.ref('/notifications/' + uid).on('value', async function(snapshot){
                var payload = JSON.stringify({
                    title: snapshot.val()['title'],
                    body: snapshot.val()['body'],
                    vibrate: [100, 50, 100]
                })
                
                await db.ref('/users/' + uid + '/subscription').once('value').then(async function(snapshot){
                    var pushSuscription = snapshot.val()

                    await webpush.sendNotification(pushSuscription, payload)
                })

                console.log('notificacion ' + payload)
            })

            // Leer dispositivos
            db.ref('/users/' + uid + '/devicesProfiles').once('value').then(function(snapshot){
                snapshot.val().forEach(function(element, index, array){
                    // Leer perfil
                    var device = element['device']
                    var profile = element['profile']

                    db.ref('/profiles/' + profile).once('value').then(function(snapshot){
                        var newObj = { name: snapshot.val()['name'], company: snapshot.val()['company'], deviceId: device }
                        perfil.push(newObj)
                    }).then(function(){

                        res.render('principal', {user: email, id: uid, profiles: perfil})
                    })
                })
            })
        }
        else    
        {
            res.redirect('/')
        }
})

app.get('/logout', async (req, res) => {
    await auth.signOut().then(function(){
        res.redirect('/')
    })
})

app.post('/login', cors(), [
    body('email')
        .isEmail().withMessage('El email es incorrecto.')
        .normalizeEmail(),
    body('password')
        .isLength({min: 5}).withMessage('La contraseña debe tener minimo 5 carácteres.')
        .not().isEmpty().withMessage('La contraseña es inválida.')
        .trim()
        .escape(),
      sanitizeBody('notifyOnReply').toBoolean()
], async (req, res) => {
    const valErrors = validationResult(req);
    var authErrors = {}

    if (valErrors.isEmpty())
    {
        await getUserAuth(req.body.email, req.body.password).then((response) => {
            authErrors = response
        })
    }

    await auth.onAuthStateChanged(function (user) {
        if (user)
        {
            res.redirect('/home')
        }
        else
        {
            res.render('home', {errores: valErrors.array(), authErrores: authErrors, email: req.body.email})
        }
    })
})

async function getUserAuth(email, password){
    var res = {}

    if (auth.currentUser) {
        await auth.signOut()
        res = {msg: ''}
    }
    else
    {
        await auth.signInWithEmailAndPassword(email, password).catch(function(error){
            var errorCode = error.code
            var errorMsg = error.message
            
            console.log('auth >> ' + errorCode)
    
            if (errorCode == 'auth/user-not-found')
            {
                // Intentar registrar nuevo usuario
                res = {msg: 'Email no registrado aún.'}
            }
            else if (errorCode == 'auth/too-many-requests')
            {
                res = {msg: 'Demasiados intentos, porfavor intente nuevamente en un momento.'}
            }
            else if (errorCode == 'auth/wrong-password')
            {
                res = {msg: 'Contraseña incorrecta.'}
            }
            else if (errorCode == 'auth/invalid-email')
            {
                res = {msg: 'El email es inválido.'}
            }
            else if (errorCode == 'auth/user-disabled')
            {
                res = {msg: 'El email ha sido deshabilitado.'}
            }
            else
            {
                res = {msg: errorMsg}
            }
        })
    }

    return res
    
}