var sensorsData = {
    dht11: {
        afterLabel: "Temperatura",
        beforeLabel: "°C"
    },
    generic: {
        afterLabel: "Sensor",
        beforeLabel: null
    }
}

module.exports = sensorsData